# prometheus

This is a Prometheus, Pushgateway, Alertmanager and Grafana stack.

## Prerequisites

A unique domain name is required by each program in the stack. (I am not a big fan of the WAR of PORTs.) So configure your DNS server first.
For instance, if your Prometheus is gonna serve at `prometheus.example.com`, this domain name should resolve to the host on which Prometheus is going to run.

This stack uses `traefik`, which is a reverse proxy. (Since four containers use four different domain names and they all are running on a single host, you gotta have a reverse proxy.)
So you should have traefik running on the host first where this stack is about to run.
Ensure that Docker network used by other containers (and traefik itself) should be named `traefik`. 

You can get traefik here: https://gitlab.com/daverona/compose/traefik

(I personally believe this is *the most difficult step* for the whole campaign.)

A plenty storage (about 20 GB for 15 days of data) is required.

## How to use

After you pull this repository, 
copy `./storage` to some place where you plan to keep the stack data.
Let's assume this directory is `/watch/prometheus/storage`.

Copy `.env.example` to `.env`. And then update `.env`:
1. Update `PROMETHEUS_DOMAIN` to your Prometheus domain name. (E.g. `prometheus.example.com`)
2. Repeat the previous step for `PUSHGATEWAY_DOMAIN`, `ALERTMANAGER_DOMAIN` and `GRAFANA_DOMAIN`.
3. Ensure that the domains map to the host on which the stack is about to run.
4. Update `PROMETHEUS_STORAGE` to the directory where you plan to keep Prometheus configuration and data.
In this case, it is `/watch/prometheus/storage/prometheus`.
5. Repeat the above step for `PUSHGATEWAY_STORAGE`, `ALERTMANAGER_STORAGE` and `GRAFANA_STORAGE`.
6. Update `PROMETHEUS_UID` to the uid who owns `/watch/prometheus/storage`.

In this example, assuming the directory owner's uid is 1001, your `.env` looks like this:

```dot-env
PROMETHEUS_TAG=v3.2.0
PROMETHEUS_TRAEFIK=prometheus
PROMETHEUS_STORAGE=/watch/prometheus/storage/prometheus
PROMETHEUS_DOMAIN=prometheus.example.com
PROMETHEUS_UID=1001
#
PUSHGATEWAY_TAG=v1.11.0
PUSHGATEWAY_TRAEFIK=pushgateway
PUSHGATEWAY_STORAGE=/watch/prometheus/storage/pushgateway
PUSHGATEWAY_DOMAIN=pushgateway.example.com
#
ALERTMANAGER_TAG=v0.28.0
ALERTMANAGER_TRAEFIK=alertmanager
ALERTMANAGER_STORAGE=/watch/prometheus/storage/alertmanager
ALERTMANAGER_DOMAIN=alertmanager.example.com
#
GRAFANA_TAG=11.5.2
GRAFANA_TRAEFIK=grafana
GRAFANA_STORAGE=/watch/prometheus/storage/grafana
GRAFANA_DOMAIN=grafana.example.com
```

Once done all the above steps, bring up the services (or containers):

```bash
docker compose up -d
```

Visit your Prometheus with its domain name. It should ask your credentials, which is:

- username: `admin`
- password: `secret` (except Grafana) or `admin` (only for Grafana)

To change it, read:

- Prometheus: `${PROMETHEUS_STORAGE}/conf/web-config.yaml` or [web-config.yaml](./storage/prometheus/conf/web-config.yaml)
- Pushgateway: `${PUSHGATEWAY_STORAGE}/conf/web-config.yaml` or [web-config.yaml](./storage/pushgateway/conf/web-config.yaml)
- Alertmanager: `${ALERTMANAGER_STORAGE}/conf/web-config.yaml` or [web-config.yaml](./storage/alertmanager/conf/web-config.yaml)
- Grafana: Whenever you sign in to Grafana with the default password, it will ask to change the password.

To read the service logs, 

```bash
docker compose logs -f
```

To shutdown all the services:

```bash
docker compose down
```


## Prometheus

Prometheus, being a data source, requires exporters from which Prometheus scrapes time-series data.
To configure exporters, please read `${PROMETHEUS_STORAGE}/conf/prometheus.yaml` or [prometheus.yaml](./storage/prometheus/conf/prometheus.yaml).

Prometheus can fire events based on alerting rules.
To configure alerting rules, please read `${PROMETHEUS_STORAGE}/conf/alert-rules.yaml` or [alert-rules.yaml](./storage/prometheus/conf/alert-rules.yaml).

There is one nice collection of Prometheus alert rules you will love:
https://samber.github.io/awesome-prometheus-alerts/rules

Whenever you update `prometheus.yaml`, `alert-rules.yaml` or `web-config.yaml`,
you can reload updated configuration without restaring Prometheus container.
Use the command below with comment `# Reload`. 
E.g. `curl -X POST -u admin:secret http://prometheus.example.com/-/reload`

```bash
# Health check
curl -X GET -u <username>:<password> <prometheus_server>[:<port>]/-/healty
# Ready
curl -X GET -u <username>:<password> <prometheus_server>[:<port>]/-/ready
# Reload
curl -X POST -u <username>:<password> <prometheus_server>[:<port>]/-/reload
```

## Pushgateway

Do *NOT* use Pushgateway unless you know what you are doing.

```bash
# Health check
curl -X GET -u <username>:<password> <pushgateway_server>[:<port>]/-/healty
# Ready
curl -X GET -u <username>:<password> <pushgateway_server>[:<port>]/-/ready
# Reload
curl -X POST -u <username>:<password> <pushgateway_server>[:<port>]/-/reload
```

## Alertmanager

Alermanager, being a notifier, requires delivery routes like email or Slack channel to let the users know what is happening.
To configure routes, please read `${ALERTMANAGER_STORAGE}/conf/alertmanager.yaml` or [alert-rules.yaml](./storage/alertmanager/conf/alertmanager.yaml).

Whenever you update `alertmanager.yaml` or `web-config.yaml`,
you can reload updated configuration without restaring Alertmanager container.
Use the command below with comment `# Reload`.
E.g. `curl -X POST -u admin:secret http://alertmanager.example.com/-/reload`


```bash
# Health check
curl -X GET -u <username>:<password> <alertmanager_server>[:<port>]/-/healty
# Ready
curl -X GET -u <username>:<password> <alertmanager_server>[:<port>]/-/ready
# Reload
curl -X POST -u <username>:<password> <alertmanager_server>[:<port>]/-/reload
```

## Grafana

To add dashboards, please visit https://grafana.com/grafana/dashboards/
and search your exporter name. Gotta love them.

## About exporters

Time to install exporters.
Please browse around: https://gitlab.com/daverona/compose/prometheus-exporters/


## References

- https://prometheus.io/docs/prometheus/latest/getting_started/
- https://prometheus.io/docs/alerting/latest/overview/
- https://velog.io/@zihs0822/Prometheus-Security
- https://grafana.com/grafana/dashboards
- https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/
- https://prometheus.io/docs/prometheus/latest/command-line/prometheus/
- https://prometheus.io/docs/prometheus/latest/management_api/
- https://prometheus.io/docs/alerting/latest/management_api/
- https://github.com/prometheus/pushgateway
- https://hub.docker.com/r/prom/pushgateway
- https://samber.github.io/awesome-prometheus-alerts/rules.html
- https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/grafana/